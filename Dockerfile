FROM ubuntu:focal
RUN echo deb http://au.archive.ubuntu.com/ubuntu/ focal main restricted > /etc/apt/sources.list                                \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-updates main restricted >> /etc/apt/sources.list                       \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal universe >> /etc/apt/sources.list                                      \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-updates universe >> /etc/apt/sources.list                              \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal multiverse >> /etc/apt/sources.list                                    \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-updates multiverse >> /etc/apt/sources.list                            \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse >> /etc/apt/sources.list \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-security main restricted >> /etc/apt/sources.list                      \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-security universe >> /etc/apt/sources.list                             \
 && echo deb http://au.archive.ubuntu.com/ubuntu/ focal-security multiverse >> /etc/apt/sources.list                           
RUN apt update

